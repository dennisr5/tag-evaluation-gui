# tag-evaluation-gui
GUI for CHILDES/RDRPOSTagger tag evaluation

## Instructions
1. Get Python 2.7 on your machine
2. In the "app" directory, run app.py with Python 2.7 (for me, that's Python app.py)
3. The application should now open
4. To open a file, go to File > Open and select a data file
5. The first sentence to be evaluated should show up in the white box
6. To navigate the file, select "Next" and "Previous" appropriately
7. To quit the program, select "Quit"
8. All files in the data directory should work (the sample ones are smaller, easier to practice with to figure out functionality)
9. Note that the processed files are in the directory data/processed

Note that if any of these steps don't work as desired, let me know what the error message is so I can try and track down what's going on.

## Known Issues
1. Application requires command line and Python 2.7 to run
2. Does not save %com lines to the output file
3. RDR has some weird tagging going on (see: line 2 in sample.rdr.proc.txt), which results in some strange mismatches and a hacky workaround to get the application to not break
4. Temporarily removed the word column until the input is sanitized better
5. Takes a while to save a very large file

If you find any issues, please feel free to add them to this list.