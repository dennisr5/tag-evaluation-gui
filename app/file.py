"""
Class that represents a file in our system
	type: type of file (CHILDES/RDR/Other)
	headers: headers in the file
	sentences: list of sentences in the file
"""
class File:
	def __init__(self, filename):
		self.filename = filename	# filename of the file
		self.getFilename()			# gets the processed file's filename if it exists
		self.getType()				# type of the file (RDR/CHILDES/Other)
		self.getHeaders()			# list of all headers in the file
		self.getSentences()			# list of all sentences in the file

	"""
	Class that represents a sentence in our file
		raw_line: raw line from file
		tags: list of tag pairs for the sentence
		sentence: actual sentence to evaluate (for display)
		label: line number and speaker
		status: whether the line has been evaluated (I/C)
	"""
	class Sentence:
		def __init__(self, raw_line):
			self.raw_line = raw_line		# the raw line associated with the sentence
			self.tags = []					# list of tag pairs for the sentence
			self.getSentenceContents()		# the actual sentence
		
		"""
		Class that represents a tag within our sentence
			raw_tag: original tag from the raw data
			processed_tag: tag processed by the application
		"""
		class TagPair:
			def __init__(self, raw_tag, processed_tag=(0,0)):
				self.raw_tag = raw_tag					# raw tag associated with the tag pair
				self.processed_tag = processed_tag		# processed tag associated with the tag pair (default empty)

			# sets the status for checkboxes for this tag
			def setProcessedTag(self, check1, check2):
				self.check1 = check1
				self.check2 = check2
				self.processed_tag = (self.check1, self.check2)

		# gets the actual sentence
		def getSentenceContents(self):
			# check if the line has a %mor line
			if "#####" in self.raw_line:
				unprocessed, processed = self.raw_line.split("#####")
				unprocessed = unprocessed.split()

				# extract line num/label and unprocessed sentence
				self.label = unprocessed[0]
				self.sentence = " ".join(unprocessed[1:])

				# extract status and tags
				self.status = processed[0]
				processed = processed[1:]
				processed = processed.split()[1:]

				# generate tag pairs for incomplete status
				if self.status is "I":
					for tag in processed:
						# completely unprocessed file
						if "#" not in tag:
							tag_pair = self.TagPair(tag)
							self.tags.append(tag_pair)
						# partially processed file
						else:
							tag, checks = tag.split("#")
							tag_pair = self.TagPair(tag)
							self.tags.append(tag_pair)

				# generate tag pairs for complete status
				else:
					for tag in processed:
						tag, checks = tag.split("#")
						tag_pair = self.TagPair(tag)

						# sets the processig information
						tag_pair.setProcessedTag(checks[0], checks[1])
						self.tags.append(tag_pair)

			# otherwise, it was not tagged, just leave it as-is
			else:
				unprocessed = self.raw_line.split()
				self.label = unprocessed[0]
				self.sentence = " ".join(unprocessed[1:])

				self.status = "I"
				self.tags = []

		# sets the processed tags
		def setProcessedTags(self, check1_arr, check2_arr):
			# TODO: Re-tag the data so that we don't have a mismatch between tags and words
			# find the smallest length out of all arrays
			length = min([len(check1_arr), len(check2_arr), len(self.tags)])

			# for each input option, map it to the corresponding tag pair and update
			for i in range(length):
				check1 = check1_arr[i].get()
				check2 = check2_arr[i].get()
				self.tags[i].setProcessedTag(check1, check2)

			# marks the sentence as completed (C)
			self.status = "C"

	# adjusts the filename if a processed file is already in progress
	def getFilename(self):
		outfile = self.filename.split("/")
		outfile.insert(-1, "processed")
		outfile = "/".join(outfile)
		try:
			open(outfile, "r")
			self.filename = outfile
		except IOError as e:
			pass

	# gets the type of the file (RDR vs CHILDES)
	def getType(self):
		if "childes" in self.filename:
			self.type = "childes"
		elif "rdr" in self.filename:
			self.type = "rdr"
		else:
			self.type = "other"

	# gets the headers from the file
	def getHeaders(self):
		self.headers = []
		fin = open(self.filename, "r")

		for line in fin:
			# all header lines start with "@"
			if line[0] == "@":
				self.headers.append(line)
			# if we reach one without it, there are no more
			else:
				break

		fin.close()

	# gets the sentences from the file
	def getSentences(self):
		self.sentences = []
		fin = open(self.filename, "r")

		for line in fin:
			if line[0] != "@":
				sentence = self.Sentence(line)
				self.sentences.append(sentence)