"""
Tag Evaluation GUI
Author: Renee Dennis
Date: 2016/03/03
"""

from file import *

try:
	# for Python2
	from Tkinter import *
	from tkFileDialog import askopenfilename
except ImportError:
	# for Python3
	from tkinter import *
	from tkinter.filedialog import askopenfilename

"""
Class that represents the GUI
"""
class Application(Frame):
	def __init__(self, master=None):
		Frame.__init__(self, master)

		self.master = master
		self.current_sentence = ""
		self.mor = []
		self.raw = []
		
		# in/outfile names
		self.infile = ""
		self.outfile = ""

		# current file object being evaluated
		self.current_file = None

		# where we're currently at in the evaluation process
		self.index = 0

		# current status of the view
		self.check1_var = dict()		# list of variables associated with first checkbox
		self.check2_var = dict()		# list of variables associated with second checkbox
		self.correction_var = dict()	# list of variables associated with correction textbox

		# flag indicating a file is currently open
		self.fileOpen = False

		# initialize application
		self.initialize()
		master.title("Tag Evaluation")

		# handler for right arrow keypress
		def right(event):
			self.onNext()

		# handler for left arrow keypress
		def left(event):
			self.onPrev()

		# initialize keypress bindings
		# for development purposes only
		# master.bind("<Right>", right)
		# master.bind("<Left>", left)

	# clears the widgets from the screen
	def clearWidgets(self):
		for label in self.master.grid_slaves():
			if int(label.grid_info()["row"]) > 0:
				label.grid_forget()

	# resets the checkmarks
	def resetChecks(self):
		raw_arr = self.current_sentence.split()
		tags = self.current_file.sentences[self.index].tags

		# if there is a mismatch, let's find the minimum size
		length = min([len(raw_arr), len(tags)])

		# reset the check arrays
		self.check1_var = [IntVar() for i in range(length)]
		self.check2_var = [IntVar() for i in range(length)]

	# sets the checkmarks based on completed data
	def setChecks(self):
		sentence_obj = self.current_file.sentences[self.index]

		# only set checkmarks if the sentence has been completed
		if sentence_obj.status == "C":
			tags = sentence_obj.tags
			length = min([len(self.check1_var), len(self.check2_var), len(tags)])
			for i in range(length):
				# set check 1
				self.check1_var[i] = IntVar()
				self.check1_var[i].set(tags[i].check1)

				# set check 2
				self.check2_var[i] = IntVar()
				self.check2_var[i].set(tags[i].check2)

	# handler for previous button press
	def onPrev(self):
		if self.index > 0:

			# set the current sentence information to the file object
			self.current_file.sentences[self.index].setProcessedTags(self.check1_var, self.check2_var)

			self.index -= 1
			self.getCurrentSentence()
			self.resetChecks()
			self.setChecks()
			self.buildSentence()

			self.saveFile();

	# handler for next button press
	def onNext(self):
		if self.current_file and self.index < len(self.current_file.sentences) - 1:	
			
			# set the current sentence information to the file object
			self.current_file.sentences[self.index].setProcessedTags(self.check1_var, self.check2_var)

			# move to the next sentence
			self.index += 1
			self.getCurrentSentence()
			self.resetChecks()
			self.setChecks()
			self.buildSentence()

			self.saveFile();

	# restarts the evaluation from the beginning
	def startOver(self):
		# only clear a file if it exists
		if self.outfile:
			open(self.outfile, 'w').close()

	# handler for saving a file
	def saveFile(self):
		# generate the outfile path
		self.outfile = self.infile.split("/")
		self.outfile.insert(-1, "processed")
		self.outfile = "/".join(self.outfile)
		self.startOver()
		fout = open(self.outfile, "a")

		# write headers
		for header in self.current_file.headers:
			fout.write(header)

		# write evaluation data
		for sentence in self.current_file.sentences:
			line = "{} {}{}{}{}".format(sentence.label, sentence.sentence, "#####", sentence.status, "%mor")
			for pair in sentence.tags:
				line += " {}#{}{} ".format(pair.raw_tag, *pair.processed_tag)
			line += "\n"
			fout.write(line)

	# handler for quit button press (also saves)
	def onQuit(self):
		if self.fileOpen and self.infile:
			self.saveFile()
		quit()

	# gets the current sentence
	def getCurrentSentence(self):
		self.current_sentence = self.current_file.sentences[self.index].sentence

	# loads a file
	def loadFile(self):
		# open file
		self.infile = askopenfilename(initialdir="data")

		self.fileOpen = True

		# check if a processed file exists for it
		self.current_file = File(self.infile)

		self.index = 0
		for sentence in self.current_file.sentences:
			# if the sentence has yet to be evaluated, start there
			if sentence.status == "I":
				break
			# otherwise it's completed and move on to the next one
			self.index += 1

		# get and build sentence
		self.getCurrentSentence()
		self.buildSentence()

	# builds the navigation buttons at the top
	def buildNav(self):
		p = Button(self.master, text=u"Previous", width=40, command=self.onPrev)
		n = Button(self.master, text=u"Next", width=40, command=self.onNext)
		q = Button(self.master, text=u"Quit", width=40, command=self.onQuit)
		p.grid(column=0, row=0, columnspan=5, pady=10)
		n.grid(column=5, row=0, columnspan=5, pady=10)
		q.grid(column=10, row=0, columnspan=5, pady=10)

	# builds the file menu
	# TODO: add start-over functionality
	def buildMenu(self):
		# root menu bar
		menubar = Menu(self.master)

		# file menu
		filemenu = Menu(menubar, tearoff=0)
		filemenu.add_command(label="Open", command=self.loadFile)
		filemenu.add_command(label="Save", command=self.saveFile)
		filemenu.add_separator()
		filemenu.add_command(label="Quit", command=self.onQuit)
		menubar.add_cascade(label="File", menu=filemenu)

		# help menu
		# menubar.add_command(label="Help")

		self.master.config(menu=menubar)

	# builds the current sentence to be displayed
	def buildSentence(self):
		# clears old widgets
		self.clearWidgets()

		# label for current sentence
		check1label = Label(self.master, text="Current Sentence")
		check1label.grid(row=1, column=0, columnspan=1, padx=10, sticky=W)

		# display the current sentence
		sentence = Label(self.master, text=self.current_sentence, height=2, width=120, bg="white", anchor=W, justify=LEFT, padx=5)
		sentence.grid(row=2, column=0, columnspan=15, pady=5)

		# build checkboxes associated with this sentence
		self.buildInputs()

	# displays the key for the user input
	# TODO: to be implemented later...
	def displayKey(self, event):
		# key = Label(self.master, text=str(event), width=20, bg="WHITE")
		# key.grid(row=5, column=11, columnspan=3, sticky=W)
		pass

	# builds the checkboxes to be displayed
	def buildInputs(self):
		# get the words
		raw_arr = self.current_sentence.split()
		mor_arr = []
		if self.current_file:
			mor_arr = self.current_file.sentences[self.index].tags

			if(self.current_file.sentences[self.index].status == "I"):
				self.resetChecks()

			# check file type
			isRdr = True if self.current_file.type == "rdr" else False

		# if there is a mismatch, let's find the minimum size
		length = min([len(raw_arr), len(mor_arr)])

		# raw label
		raw_label = Label(self.master, text="Word")
		raw_label.grid(row=4, column=0, columnspan=2, padx=10, sticky=W)

		# mor label
		mor_label = Label(self.master, text="Tag")
		mor_label.grid(row=4, column=2, columnspan=6, padx=10, sticky=W)

		# checkbox 1 label
		check1_label = Label(self.master, text="Main")
		check1_label.grid(row=4, column=8, columnspan=1, sticky=W)

		# checkbox 2 label
		check2_label = Label(self.master, text="Other")
		check2_label.grid(row=4, column=9, columnspan=1, sticky=W)

		j = 0	# keeps track of last row with a widget in it
		for i in range(length):

			# TODO: Implement this when we figure out how to overcome the lack
			# of one-to-one correspondence

			if(isRdr):
				mor_val, raw_val = mor_arr[i].raw_tag.split("|")
			else:
				raw_val = raw_arr[i]
				mor_val = mor_arr[i].raw_tag

			# raw
			raw = Label(self.master, text=raw_val)
			raw.grid(row=5+i, column=0, columnspan=2, padx=10, sticky=W)

			# mor
			mor = Label(self.master, text=mor_val)
			mor.grid(row=5+i, column=2, columnspan=6, padx=10, sticky=W)

			# checkbox 1
			check1 = Checkbutton(self.master, variable=self.check1_var[i])
			check1.grid(row=5+i, column=8, columnspan=1, sticky=W)

			# checkbox 2
			check2 = Checkbutton(self.master, variable=self.check2_var[i])
			check2.grid(row=5+i, column=9, columnspan=1, sticky=W)

			# TODO: implement correction functionality
			# TODO: implement correction key

			j = i

		# current status
		try:
			status_str =  "{} of {}".format(self.index + 1, len(self.current_file.sentences))
		except:
			status_str = "Please load a file."

		status = Label(self.master, text=status_str)
		status.grid(row=6+j, column=0, columnspan=15, pady=10)

	# initializes the application view
	def initialize(self):
		self.grid()
		self.buildMenu()
		self.buildNav()
		self.buildSentence()

root = Tk()
app = Application(master=root)
app.mainloop()